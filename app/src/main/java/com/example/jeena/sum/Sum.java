package com.example.jeena.sum;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Sum extends AppCompatActivity implements View.OnClickListener {
    EditText et_number1, et_number2;
    TextView txtResult;
    Button btn_sum;
    int no1, no2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sum);

        et_number1 = (EditText) findViewById(R.id.et_number1);
        et_number2 = (EditText) findViewById(R.id.et_number2);
        btn_sum = (Button) findViewById(R.id.btn_sumbutton);
        btn_sum.setOnClickListener(this);
        txtResult = (TextView) findViewById(R.id.txt_result);

        clear();

    }

    public void clear()
    {
        et_number1.setText("");
        et_number2.setText("");
    }

    @Override
    public void onClick(View view) {

        if (view == btn_sum)
        {
            no1 = Integer.parseInt(et_number1.getText().toString());
            no2 = Integer.parseInt(et_number2.getText().toString());
            int result = no1 + no2;
            Log.d("asd", "onClick: "+result);
            txtResult.setText(""+result);

        }


    }
}

